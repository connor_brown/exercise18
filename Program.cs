﻿using System;
// a)  Create 5 variables and print to the screen the
// 		name of the variable and then the value.
// 	Example: “The value of num1 = 5” 


// b)	Create 6 variables and do the same as task a,
// 	but create only 3 lines where you are adding
// 	pairs of numbers together
// 	Example:	“num1 + num2 = 34”

// c)	Create 4 variables and add them together, by
// 	placing the answer in the original variable

// 	Note: Do this with the shorthand code
// 	Example the longhand version is: 
// “num1 = num1 + num2”

namespace exercise18
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = (1);
            int num2 = (2);
            int num3 = (3);
            int num4 = (4);
            int num5 = (5);
            int num6 = (6);


            Console.WriteLine($"1 = {num1}");
            Console.WriteLine($"2 = {num2}");
            Console.WriteLine($"3 = {num3}");
            Console.WriteLine($"4 = {num4}");
            Console.WriteLine($"5 = {num5}");
            Console.WriteLine($"{num1} + {num6} = {num1 + num6}");
            Console.WriteLine($"{num2} + {num5} = {num2 + num5}");
            Console.WriteLine($"{num3} + {num4} = {num3 + num4}");
            Console.WriteLine(num1 = num1+num2+num3+num4);
        }
    }
}
